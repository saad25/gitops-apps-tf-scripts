terraform {
  required_version = ">= 0.14.0"

  required_providers {
    spectrocloud = {
      version = "0.5.7"
      # version = ">= 0.5.0"
      source = "spectrocloud/spectrocloud"
    }
    # aws = {
    #   source  = "hashicorp/aws"
    #   version = "~> 3.0"
    # }
  }

  backend "http" {

  }
  # backend "s3" {
  #   bucket = "boobalan"
  #   key = "ge/terraform.tfstate"
  #   region = "us-east-1"
  # }
}

# Spectro Cloud
variable "sc_host" {}
variable "sc_username" {}
variable "sc_password" {}
variable "sc_project_name" {}

provider "spectrocloud" {
  host         = var.sc_host
  username     = var.sc_username
  password     = var.sc_password
  project_name = var.sc_project_name
}


variable "team" {
  type = string
}

variable "app_replicas" {
  type    = string
  default = "2"
}
