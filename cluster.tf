resource "spectrocloud_cluster_eks" "dev" {
  name = "eks-dev-4"

  cluster_profile {
    id = "615d11913faf318a1cd504f9"

    # pack {
    #   name   = data.spectrocloud_pack.k8s.name
    #   tag    = data.spectrocloud_pack.k8s.version
    #   values = local.k8s_values
    # }

    # pack {
    #   name   = data.spectrocloud_pack.byom.name
    #   tag    = data.spectrocloud_pack.byom.version
    #   values = ""
    #   /*values = <<-EOT
    #     manifests:
    #       byo-manifest:
    #         contents: |
    #         ${indent(4, trim(yamlencode(join("", values({for k in each.value.active-profile-installation : k => replace(var.profileinstallationtemplate, "{{PROFILE_NAME}}", k)}))), "|"))}

    #   EOT*/
    # }

    # pack {
    #   name   = data.spectrocloud_pack.aws-alb.name
    #   tag    = data.spectrocloud_pack.aws-alb.version
    #   values = templatefile("pack-config/aws-alb-ingress.yaml",{
    #     vpcId : module.core["core-${each.value.env}"].aws_vpc_main_id
    #   })
    # }
  }

  cluster_profile {
    id = "615d115f3faf3189f46a82a5"
  }

  cluster_profile {
    id = local.profile_ids["team1-dev-apps1"]
  }

  cluster_profile {
    id = local.profile_ids["team1-dev-apps2"]
  }

  cloud_account_id = "60b9150ce937eaa26f213e0f"

  cloud_config {
    # ssh_key_name = var.cluster_ssh_public_key_name
    region              = "us-east-1"
    vpc_id              = "vpc-0c9679602584608f9"
    az_subnets          = { "us-east-1a" : "subnet-080b9dc42d15b57a2", "us-east-1b" : "subnet-025428faaddc4201f" }
    azs                 = []
    public_access_cidrs = []
    endpoint_access     = "public"
  }

  # backup_policy {
  #   schedule                  = each.value.backup_policy.schedule
  #   backup_location_id        = local.bsl_ids[each.value.backup_policy.backup_location]
  #   prefix                    = each.value.backup_policy.prefix
  #   expiry_in_hour            = 7200
  #   include_disks             = true
  #   include_cluster_resources = true
  # }

  backup_policy {
    backup_location_id        = "60c766724a74c89881ebcf15"
    expiry_in_hour            = 730
    include_cluster_resources = false
    include_disks             = false
    namespaces                = []
    prefix                    = "test123"
    schedule                  = "0 0 1 */2 *"
  }

  # scan_policy {
  #   configuration_scan_schedule = each.value.scan_policy.configuration_scan_schedule
  #   penetration_scan_schedule   = each.value.scan_policy.penetration_scan_schedule
  #   conformance_scan_schedule   = each.value.scan_policy.conformance_scan_schedule
  # }

  # pack {
  #   name = "kubernetes"
  #   tag  = var.cluster_packs["k8s"].tag
  #   values = templatefile(var.cluster_packs["k8s"].file, {
  #     certSAN: "api-${local.fqdn}",
  #     issuerURL: "dex.${local.fqdn}",
  #     etcd_encryption_key: random_id.etcd_encryption_key.b64_std
  #   })
  # }

  machine_pool {
    name          = "workers"
    count         = 2
    instance_type = "t3.large"
    az_subnets = {
      us-east-1a = "subnet-07f0af093b8233990"
      us-east-1b = "subnet-0e0d96dc7ad3f02a5"
    }
    disk_size_gb = 62
    azs          = []
  }
}

