locals {
  profiles_files = fileset("${path.module}/config", "profile-*.yaml")
  cluster_profiles = {
    for k in local.profiles_files :
    trimsuffix(k, ".yaml") => yamldecode(templatefile("config/${k}", {
      team         = var.team
      app_replicas = var.app_replicas
    }))
  }


  profile_ids = merge({
    }, {
    for k, v in spectrocloud_cluster_profile.ehl :
    v.name => v.id
  })
}

resource "spectrocloud_cluster_profile" "ehl" {
  for_each = local.cluster_profiles
  name     = each.value.name

  description = each.value.description
  type        = each.value.type

  dynamic "pack" {
    for_each = each.value.packs
    content {
      name   = pack.value.name
      type   = pack.value.type
      values = <<-EOT
        pack:
          spectrocloud.com/install-priority: "${pack.value.install-priority}"
      EOT

      dynamic "manifest" {
        for_each = pack.value.argo_manifests
        content {
          name    = manifest.value.name
          content = <<-EOT
            apiVersion: argoproj.io/v1alpha1
            kind: Application
            metadata:
              name: ${manifest.value.name}
              namespace: argocd
              finalizers:
              - resources-finalizer.argocd.argoproj.io
            spec:
              destination:
                server: 'https://kubernetes.default.svc'
                namespace: ${var.team}
              source:
                repoURL: ${lookup(manifest.value.source, "repoURL", each.value.defaultRepoURL)}
                chart: ${manifest.value.source.chart}
                targetRevision: ${manifest.value.source.version}
                helm:
                  parameters: ${jsonencode(manifest.value.parameters)}
              project: default
              syncPolicy:
                automated:
                  selfHeal: false
                  prune: true
                syncOptions:
                - CreateNamespace=true
          EOT
        }
      }
    }
  }
}
